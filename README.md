# Migrant - Debian to Devuan transformer

You need to read the script to learn about it.

## Version

0.1 α

## Testing

It has been tested on these two simple environments:

- the official Vagrant image debian/bullseye64
- a manually configured image of Bullseye with LVM over LUKS and Gnome

The results seem fine.

## License

GPL 3.0 or later